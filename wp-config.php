<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */


@ini_set( 'upload_max_size' , '1200M' );

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mama' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'IvZ5hb2itiyL&wEa%KKNjJ+IGmF]oh TD#eeihxj V?%{$Tw<QpZ^6SKK;},|HDu' );
define( 'SECURE_AUTH_KEY',  'uIuQNyi!^zA.r)?M9}Hl78xA#iB^ADU^CEDnefEGhO<A~Ih#dr:F{>n#RLeDMW>M' );
define( 'LOGGED_IN_KEY',    '3/:R;?pRY!3n!?O xon?q;wyxvxbH$`x|r?ep+hrN>!%)dQG>))]$CeDv/djkxE.' );
define( 'NONCE_KEY',        '2XoR.VPkZWnyV9p7zB0@_n8!9lrqiLP`6oJMU/`w+}CtU8V_$CLb/`GdBfN#$o)E' );
define( 'AUTH_SALT',        ';3Fp~Rllbed{+)+00/lk/x}B)dF[W %#]%mlkv$OQWp1-+ju5|IlmS-)b_E$qJI-' );
define( 'SECURE_AUTH_SALT', '}ch-a/(UHONzd J{12yOT|;Sn>h_4;_{v?g-Y-kj04h>4]n}z-8HU7Ck!!wSCHi$' );
define( 'LOGGED_IN_SALT',   '%cw|*1/+ v~][o^FCq2BHA@$ {xm<)rs;4k7dVS@l[A%A`nuL:EklO(6:ZZ4+KJ=' );
define( 'NONCE_SALT',       '**yM{+kR5Yq<Y)LwK D&A2q<RFqfB:`&nTh` %_E k/ 8E(&5Pte@<0xo]2#z88f' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
